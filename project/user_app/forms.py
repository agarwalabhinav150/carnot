from django import forms


class StudentSearchForm(forms.Form):
    search = forms.CharField(label='search', widget=forms.TextInput(
        attrs={'placeholder': 'Enter student id or name'}))
