# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import OrderedDict

from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.models import User
from django.views.generic import View
from django.http import HttpResponse

from constants import *

from .forms import StudentSearchForm

from . models import Profile, School, UserBookMapping

# Create your views here.


def get_detail(user):
    data = OrderedDict()
    data = {
        "Full Name": user.get_full_name(),
        "Email": user.email,
        "Gender": GENDER_CHOICES.get(user.profile.gender, None)

    }
    if user.profile.school is not None:
        data['School Name'] = user.profile.school.name
        data['School Phone'] = user.profile.school.phone
    if None not in user.userbookmapping_set.values_list(
            'book__name', flat=True):
        data['Books Read'] = ', '.join(
            user.userbookmapping_set.values_list(
                'book__name', flat=True))
    return data


class StudentDetail(View):

    template_name = "view/view_student.html"

    def get(self, request, **kwargs):
        user = User.objects.filter(id=kwargs['student_id']).select_related(
            'profile', 'profile__school')
        if user.exists():
            user = user.first()
            data = get_detail(user)
            return render(request, self.template_name, {'data': data})
        else:
            return HttpResponse("Student with given id was not found.")


class StudentSearch(View):

    template_name = "forms/search_student.html"
    success_url = '/search_student'
    form_class = StudentSearchForm

    def get(self, request):
        form = self.form_class
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            search_key = form.cleaned_data.get('search')
            if search_key in ['1', '2']:
                return render(request, self.template_name, {
                    'form': form, 'error': 'No user found with matching criteria.'})
            users = User.objects.filter(
                Q(id__contains=search_key) |
                Q(first_name__contains=search_key) |
                Q(last_name__contains=search_key))
            if users.exists():
                user_list = list()
                for user in users:
                    user_list.append(get_detail(user))
                return render(request, self.template_name, {
                    'form': form, 'data': user_list})
            else:
                return render(request, self.template_name, {
                    'form': form,
                    'error': 'No user found with matching criteria.'})
        else:
            return render(request, self.template_name, {'form': form})
