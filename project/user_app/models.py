# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.


class ActiveModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Book(ActiveModel):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Book"
        verbose_name_plural = "Books"

    def __unicode__(self):
        return "%s__%s" % (str(self.id), str(self.name))


class School(models.Model):
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=100, default='fake phone no')

    class Meta:
        verbose_name = "School"
        verbose_name_plural = "Schools"

    def __unicode__(self):
        return "%s__%s" % (str(self.id), str(self.name))


class Profile(ActiveModel):

    GENDER_CHOICES = (
        ('Male', 1),
        ('Female', 2),
    )
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING)
    gender = models.IntegerField(blank=True, null=True, choices=GENDER_CHOICES)
    school = models.ForeignKey(
        School, on_delete=models.DO_NOTHING, blank=True, null=True)

    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"

    def __unicode__(self):
        return "%s__%s" % (str(self.id), str(self.user.email))


class UserBookMapping(ActiveModel):
    book = models.ForeignKey(Book, db_index=False,
                             on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, db_index=False,
                             on_delete=models.CASCADE)

    class Meta:
        unique_together = (
            'book',
            'user',)

    def __unicode__(self):
        return "%s books" % (str(self.user.email))
